# RMT Extra Learning - Student AMI

## Why?

RMT is now entering the modules where we will be changing the Flask app often. Each change results in a new AMI to be deployed.

Rather than seeing the students hand jam their way to an AMI each week, I decided to packerize it. For those that do not know, Packer is one of my top three DevOps tools of all time. Pick any kind of virtual image you want and Packer can make it easier for you to build.


## What's On it?

This list is based on RMT being a Python shop.

* Amazon Linux
* Python 3.6
* Pip: Flask
* Pip: Flask-WTF
* Pip: redis
* Pip: gunicorn
* Pip: Ansible
* Pip: boto
* Pip: boto3
* Terraform
* Terragrunt

Those are all of the tools RMT has been using so far in its extra learning assignments.

## Installing Packer

On Mac:

* brew install packer

Download:

* [From] (https://github.com/hashicorp/packer/releases)

## Building Your Own AMI

You will need to export AWS environment variables for the following Packer template variables:

* AWS\_ACCESS\_KEY_ID
* AWS\_SECRET\_ACCESS_KEY
* AWS\_DEFAULT_REGION

For each Packer command with the template you have to pass a **code_artifact** variable. That variable needs to reference the path to a zip file of your code. Yes, the code must be zipped even if it is a single .py file.

You will also need to pass a **student_name** variable. This will allow several students under one teacher to build AMIs without naming conflicts.

RMT did not want to push binaries to GitHub so we have one **terragrunt_binary** and one **terraform_artifact** variable that you will need to set. These need to point to the files your downloaded for each.

### Validating the Template

It is always a good idea to validate your template before building it. Validation does not need to use expensive AWS  resources to figure out if the template is bad.

```
packer validate -var 'code_artifact=/Users/mary/mycode.zip' -var 'student_name=Mary' -var 'terragrunt_binary=./files/terragrunt_linux_amd64' -var 'terraform_artifact=./files/terraform_0.11.4_linux_amd64.zip' el_student_ami.json
```

### Building an AMI

Once you know your template is valid, you can try to build an AMI. This may take a few tries as most of us cannot fully think through provisioner steps ahead of time.

I often stand an instance up manually so I can test my proposed provisioner steps. Once I have the correct set, I paste them into my template.

```
packer build -var 'code_artifact=/Users/john/mycode.zip' -var 'student_name=John' -var 'terragrunt_binary=./files/terragrunt_linux_amd64' -var 'terraform_artifact=./files/terraform_0.11.4_linux_amd64.zip' el_student_ami.json
```

On success, Packer will provide your AMI ID. Once you have that you can stand instances up from it, whether you are using the AWS console, Terraform, Ansible, Chef, Salt or Puppet.

That is one reason why images are my currency of choice. They give you the flexibility to use your favorite tool with them.

### Automatically Starting Your App

The AMI has gunicorn setup to start when the instance boots. It is setup so gunicorn runs **/home/ec2-user/flask_app.py** and pairs it with app, i.e. **flask_app:app**. If you setup your code the same way you will no longer need to SSH to your private instance to run Flask.
