# rmt-extra-learning-documents

This repo contains the documents and associated source code and files RMT uses for its extra learning program with its co-op students

## What the heck is extra learning?

RMT hired its student employees first and foremost to develop products for our business. Products tend to have a narrow focus.

Rick, an RMT co-owner, believes people should have a wide skill set so we added the extra learning to broaden our student employees skill sets.

## Why are these public?

We are releasing the documents to the public in case other teachers want to run a similar program. Most of the content in this falls under ideas we have for an RMT DevOps Academy. That would essentially be a training program we run under our company. It just takes more money and time than we have these days so it is time to make the tent bigger.

## Just in Time Documents

RMT develops its modules weekly. That allows us to see how our student employees did with the prior module. This way we can adjust according to the current abilities of our students. This works because we have a maximum of three students at any one time.

We won't release a new module to the public until our students have had a chance to do them. That tells us what worked and did not work so we can adjust the module for the public version.

## Technical Areas

RMT designed its extra learning program around three technical areas.

* Cloud & Configuration Management
* Containers
* Serverless

We started with Cloud & Configuration Management as that is the area most important to the first product we are building.

RMT has 24 modules in mind across all three areas. We don't believe our current student employees will get through all 24 in their time with us. We will make a decision at the end of the program on releasing modules our student employees did not test.

## Books

As a company, we love books. So much so that we bought our employees books we felt would help them most with the products they are developing and the extra learning assignments. When you see books referenced in the extra learning assignments, they come from this list.

* **The Python Apprentice**, Sixty North, Leanpub
* **The Python Journeyman**, Sixty North, Leanpub
* **The Python Master**, Sixty North, Leanpub
* **Terraform Up & Running**, Yevgeniy Brikman, O'Reilly
* **Ansible for DevOps**, Jeff Geerling, Leanpub
* **The Kubernetes Book**, Nigel Poulton, Amazon

RMT is also picky about its authors, so there is a reason behind the books we chose. As a Python team, we greatly admire the Python skills at Sixty North. When Rick was an Ansible contributor he admired Jeff Geerling's Ansible skills and Yevgeniy Brikman's skills stand on their own as part of the Gruntworks group.

When we got to Docker, Rick had two experts in mind - Nigel Poulton and Kelsey Hightower. We chose Nigel's book as it was more geared to people learning Kuberenetes for the first time.

## Mix of Hand Jams and Automation

People that know Rick best know he abhors hand jams. However, teaching these modules was a little different.

Situations would occur while the students were working on a module and it would be easier to get them unblocked using the AWS CLI or console than having them continually update their Terraform or Ansible code.

This is why we had them on-site for extra learning days. It gave Rick a chance to explain best practice while helping them hand jam for time reasons.

## Contacting RMT

RMT's co-owners: Rick and Ginny Mendes

Phone: 877-766-6580

Company Email: info@rickmendes.tech

Company Site: https://rickmendes.tech

## License

* MIT